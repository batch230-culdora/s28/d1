// CRUD operations (Create, Read, Update and Delete)
/*
	-CRUD operations are the heart of any backend application
	-Mastering the CRUD operations is essential for any developer
	-This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
	-Mastering the CURD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information
*/

// [SECTION] Inserting documents (CREATE)
/*
	Syntax:
		- db.collectionName.insertOne({object});

*/

// Insert One (Inserts one document)
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact:{
		phone: "87654321",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

// Insert Many (Insert one or more documents)
/*
	-Syntax
		db.collectionName.insertMany([{objectA},{objectB}]);

*/
db.users.insertMany(
	[
		{	
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87000",
				email: "stephenhawking@mail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "4770220",
				email: "neilarmstrong@mail.com"
			},
			courses: ["React", "Laravel", "MongoDB"],
			department: "none"
		}

		]
	)

// [SECTION] finding documents (READ)
/*
	-db.collectionName.find();
	-db.collectionName.find(field: value);
*/

// Retrieves all documents
db.users.find();
// Retrieves specific document/s
db.users.find({firstName: "Stephen"});
// Multiple criteria
db.users.find({lastName: "Armstrong", age:82});


// [SECTION] Update and replace (UPDATE)

db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});

/*
	Syntax:
	-db/colleactionName.updateOne({Criteria}, {$set: {field/s : value/s}})
*/
	
db.users.updateOne(
	{ firstName: "Test" },
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "imrich@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
)

db.users.updateOne(
	{firstName: "Jane"},
	{
		$set: 
		{
			firstName:"Jenny"
		}
	}
)
  
db.users.updateMany(
	{department: "none"},
	{
		$set:
	{
		department: "HR"
		}

	}
)

db.users.replaceOne(
	{firstName: "Bill"}, //criteria
	{
		firstName: "Elon",
		lastName: "Musk",
		age: 30,
		courses: ["PHP", "Laravel","HTML"],
		status: "trippings"
	}

)






// {[SECTION]} delete(delete)
/*
	db.collectionName.deleteOne({field: value})
	db.collectionName.deleteMany({field: value})
*/

db.users.deleteOne({
	firstName: "Jane";
})
db.users.deleteMany({
	firstName: "Jane";
})

db.users.deleteMany({
	department: "HR"	
})

